locals {
  bucket_name = "solaris2451-aws-terraform-eks-with-argocd-state-bucket"
}

resource "aws_iam_role_policy" "gitlab_ci" {
  name   = "s3"
  role   = aws_iam_role.gitlab_ci.name
  policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Effect    = "Allow"
        Action    = [
          "ec2:CreateTags",
          "ec2:DeleteTags"
        ]
        Resource = "*"
      },
      {
        Effect    = "Allow"
        Action    = [
          "eks:*",
          "ecs:*",
          "route53:*",
          "s3:*",
          "acm:*",
          "ecr:*",
          "ec2:*",
          "elasticloadbalancing:*",
          "iam:*",
          "logs:*",
          "cloudwatch:*",
          "autoscaling:*",
          "sts:*",
          "sns:*",
          "cloudformation:*",
          "datapipeline:*",
          "firehose:*",
          "kinesis:*",
          "lambda:*",
          "rds:*",
          "redshift:*",
          "sqs:*",
          "swf:*"
        ]
        Resource = "*"
      },
      {
        Effect    = "Allow"
        Action    = "ecr:GetAuthorizationToken"
        Resource = "*"
      },
      {
        Effect    = "Allow"
        Action    = [
          "eks:ListClusters",
          "eks:DescribeCluster",
          "ecs:ListClusters",
          "ecs:DescribeClusters",
          "ecs:ListServices",
          "ecs:DescribeServices",
          "ecs:ListTaskDefinitions",
          "ecs:DescribeTaskDefinition",
          "ecs:DescribeTaskDefinition",
          "ecr:DescribeRepositories",
          "ecr:DescribeImages",
          "ecr:ListImages",
          "ecr:BatchGetImage",
          "logs:DescribeLogGroups",
          "logs:GetLogEvents",
          "cloudwatch:GetMetricStatistics",
          "autoscaling:DescribeAutoScalingGroups",
          "rds:DescribeDBInstances"
        ]
        Resource = "*"
      },
      {
        Effect    = "Allow"
        Action    = [
          "acm:DescribeCertificate",
          "acm:ListCertificates"
        ]
        Resource = "*"
      },
      {
        Effect = "Allow"
        Action = [
          "kms:*"
        ]
        Resource = "*"
      },
      {
        Effect    = "Allow"
        Action    = [
          "vpc:*"
        ]
        Resource = "*"
      }
    ]
  })
}


resource "aws_iam_role_policy" "gitlab_ci_ecr" {
  name   = "ecr"
  role   = aws_iam_role.gitlab_ci_ecr.name
  policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
    {
      "Effect": "Allow",
      "Action": [
          "ecr:GetAuthorizationToken",
          "ecr:BatchCheckLayerAvailability",
          "ecr:GetDownloadUrlForLayer",
          "ecr:BatchGetImage",
          "ecr:PutImage",
          "ecr:InitiateLayerUpload",
          "ecr:UploadLayerPart",
          "ecr:CompleteLayerUpload",
          "ecr:DescribeRepositories",
          "ecr:CreateRepository",
          "ecr:TagResource"
      ],
      "Resource": "*"
    }
    ]
  })
}