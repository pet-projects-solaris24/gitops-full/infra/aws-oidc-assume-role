output "role_arn" {
  description = "Role arn that needs to be assumed by GitLab CI"
  value       = aws_iam_role.gitlab_ci.arn
}

output "role_arn2_ecr" {
  description = "Role arn that needs to be assumed by GitLab CI"
  value       = aws_iam_role.gitlab_ci_ecr.arn
}