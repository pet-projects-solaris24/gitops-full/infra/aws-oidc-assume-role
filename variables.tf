variable "gitlab_group" {
  type = string
  description = "Gitlab group name"
  default = "pet-projects-solaris24"
}

variable "gitlab_subgroup1" {
  type = string
  description = "Gitlab subgroup name"
  default = "gitops-full"
}

variable "gitlab_subgroup1_1" {
  type = string
  description = "Gitlab subgroup name"
  default = "infra"
}

variable "gitlab_subgroup2_1" {
  type = string
  description = "Gitlab subgroup name"
  default = "apps"
}


variable "gitlab_project" {
  type = string
  description = "Gitlab project name"
  default = "aws-terraform-eks-with-argocd"
}


variable "gitlab_branch" {
  type = string
  description = "Gitlab project branch name"
  default = "main"
}

variable "gitlab_url" {
    type = string
  description = "Gitlab url"
  default = "https://gitlab.com"
}

variable "role_name" {
  type = string
  description = "Gitlab iam role name"
  default = "gitlab-ci"
}


variable "role_name_ecr" {
  type = string
  description = "Gitlab iam role name"
  default = "gitlab-ci_ecr"
}